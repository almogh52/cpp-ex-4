#include "FileStream.h"

/*
* FileStream class constructor
* param fileName: The path to the file
*/
FileStream::FileStream(char *fileName)
{
    // Try to open the given file stream in append mode in order to add to it's end
    _streamFile = fopen(fileName, "a");

    // If the file couldn't be opened print error to stderr
    if (!_streamFile)
    {
	fprintf(stderr, "Unable to open file for writing!\n");
    }
}

/*
* FileStream class destructor
*/
FileStream::~FileStream()
{
    // Close the stream file
    fclose(_streamFile);

    _streamFile = nullptr;
}
