#pragma once
#include <stdlib.h>
#include "OutStream.h"

class FileStream : public OutStream
{
public:
    FileStream(char *fileName);
    ~FileStream();
};

