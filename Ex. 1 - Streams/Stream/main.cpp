#include <Windows.h>
#include "OutStreamEncrypted.h"
#include "OutStream.h"
#include "FileStream.h"
#include "Logger.h"

int main(int argc, char **argv)
{
    // Create a stream for stdout
    OutStream cout = OutStream();

    // Write to the console the wanted line and a new line after
    cout << "I am the Doctor and I'm " << 1500 << " years old" << endline;

    // Create a stream for a file
    FileStream fileStream = FileStream("some_file.txt");

    // Write to the file the wanted line and a new line after
    fileStream << "I am the Doctor and I'm " << 1500 << " years old" << endline;

    // Create an encrypted stream for stdout
    OutStreamEncrypted coutEncrypted = OutStreamEncrypted(5);

    // Write to the console the wanted line and a new line after
    coutEncrypted << "I am the Doctor and I'm " << 1500 << " years old" << endline;

    // Create 2 objects of the logger class
    Logger l1 = Logger();
    Logger l2 = Logger();

    // Check operators on both loggers
    l1 << "Hello trying first line in l1, Some number: " << 1500 << endline;
    l2 << "Hell trying first line in l2, Some number: " << 1500 << endline;

    l1 << "Trying second line in l1" << endline;
    l2 << "Trying second line in l2" << endline;

    system("pause");

    return 0;
}
