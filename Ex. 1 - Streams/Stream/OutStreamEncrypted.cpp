#include "OutStreamEncrypted.h"

OutStreamEncrypted::OutStreamEncrypted(int offset) : _offset(offset) { }

/*
* This function adds the encrypt offset to a char
* param c: The char to be encrypted
* Return value: The char encrypted
*/
char OutStreamEncrypted::addOffsetToChar(char c)
{
    // Check if the character is in encrypt range
    if (c >= CHAR_RANGE_MIN || c <= CHAR_RANGE_MAX)
    {
	// Calc the new character with the offset
	c += this->_offset;

	// If the character is bigger than the max of the range, calc it from the start
	if (c > CHAR_RANGE_MAX)
	{
	    c = c % CHAR_RANGE_MAX + CHAR_RANGE_MIN;
	}
    }

    return c;
}

/*
* This operator allows for string encrypted streaming
* param str: The string to be streamed
* Return value: The ref to the stream object
*/
OutStreamEncrypted & OutStreamEncrypted::operator<<(const char * str)
{
    // Allocate a new array with the same size
    char *strConverted = new char[strlen(str) + 1];

    // Go through the character in the old array and convert them
    for (int i = 0; i < strlen(str); i++)
    {
	// Convert the current character and set it in the new allocated array
	strConverted[i] = this->addOffsetToChar(str[i]);
    }

    // Set the end of the string
    strConverted[strlen(str)] = 0;

    // Using the parent's function, print the converted string
    OutStream::operator<<(strConverted);

    // Free memory
    delete strConverted;

    return *this;
}

/*
* This operator allows for integer encrypted streaming
* param num: The integer to be streamed
* Return value: The ref to the stream object
*/
OutStreamEncrypted & OutStreamEncrypted::operator<<(int num)
{
    // Convert the number to a string using std string function and convert it to char string
    std::string numStr = std::to_string(num);

    // Use the normal encrypt function to print the number
    this->operator<<(numStr.c_str());

    return *this;
}

/*
* This operator allows for function streaming
* param pf: The pointer to the function to be streamed
* Return value: The ref to the stream object
*/
OutStreamEncrypted & OutStreamEncrypted::operator<<(void(*pf)(FILE *streamFile))
{
    // Call the same operator from the parent since there is no need for encryption here
    OutStream::operator<<(pf);

    return *this;
}

