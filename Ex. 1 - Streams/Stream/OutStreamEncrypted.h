#pragma once
#include <string.h>
#include <string>
#include "OutStream.h"

#define CHAR_RANGE_MIN 32
#define CHAR_RANGE_MAX 126

class OutStreamEncrypted : public OutStream
{
protected:
    int _offset;

private:
    char OutStreamEncrypted::addOffsetToChar(char c);

public:
    OutStreamEncrypted(int offset);
    OutStreamEncrypted & operator<<(const char * str);
    OutStreamEncrypted & operator<<(int num);
    OutStreamEncrypted & operator<<(void(*pf)(FILE *streamFile));
};

