#include "Logger.h"

/*
* This function prints the start line signuature
* Return value: None
*/
void Logger::setStartLine()
{
    static unsigned int currentLine = 0;

    // Increase the current line
    currentLine++;

    // Print to the stream LOG as start line
    os << "LOG " << currentLine << ": ";

    // Reset start line boolean
    _startLine = false;
}

/*
* Logger class constructor
*/
Logger::Logger()
{
    // Call the constructor of OutStream to init it
    os = OutStream();

    // Set the first line as start line
    _startLine = true;
}

/*
* This operator allows for string streaming
* param l: The ref to the Logger object
* param msg: The string to be streamed
* Return value: The ref to the stream object
*/
Logger & operator<<(Logger & l, const char * msg)
{
    // If it's a start line, call set start line function
    if (l._startLine)
    {
	l.setStartLine();
    }

    // Normal print to stream
    l.os << msg;

    return l;
}

/*
* This operator allows for integer streaming
* param l: The ref to the Logger object
* param num: The integer to be streamed
* Return value: The ref to the stream object
*/
Logger & operator<<(Logger & l, int num)
{
    // If it's a start line, call set start line function
    if (l._startLine)
    {
	l.setStartLine();
    }

    // Normal print to stream
    l.os << num;

    return l;
}

/*
* This operator allows for function streaming
* param l: The ref to the Logger object
* param pf: The pointer to the function to be streamed
* Return value: The ref to the stream object
*/
Logger & operator<<(Logger & l, void(*pf)(FILE *streamFile))
{
    // Set the next as a start line
    l._startLine = true;

    // Normal print to stream
    l.os << pf;

    return l;
}
