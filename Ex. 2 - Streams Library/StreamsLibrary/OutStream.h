#pragma once
#include <stdio.h>

namespace msl {
    class OutStream
    {
    protected:
	FILE * _streamFile;

    public:
	OutStream();

	OutStream& operator<<(const char *str);
	OutStream& operator<<(int num);
	OutStream& operator<<(void(*pf)(FILE *streamFile));
    };

    void endline(FILE *streamFile);
}