#include "OutStream.h"

namespace msl {
    /*
    * OutStream class constructor
    */
    OutStream::OutStream()
    {
	// Set the output of the stream as stdout
	_streamFile = stdout;
    }

    /*
    * This operator allows for string streaming
    * param str: The string to be streamed
    * Return value: The ref to the stream object
    */
    OutStream& OutStream::operator<<(const char *str)
    {
	// If the file stream isn't null
	if (_streamFile)
	{
	    fprintf(_streamFile, "%s", str);
	}
	return *this;
    }

    /*
    * This operator allows for integer streaming
    * param num: The integer to be streamed
    * Return value: The ref to the stream object
    */
    OutStream& OutStream::operator<<(int num)
    {
	// If the file stream isn't null
	if (_streamFile)
	{
	    fprintf(_streamFile, "%d", num);
	}
	return *this;
    }

    /*
    * This operator allows for function streaming
    * param num: The pointer to the function to be streamed
    * Return value: The ref to the stream object
    */
    OutStream& OutStream::operator<<(void(*pf)(FILE *streamFile))
    {
	// If the file stream isn't null
	if (_streamFile)
	{
	    pf(_streamFile);
	}
	return *this;
    }

    /*
    * This function prints new line to a file
    * param streamFile: The stream file to print new line in it
    */
    void endline(FILE *streamFile)
    {
	// Print the new line to the file stream
	fprintf(streamFile, "\n");
    }
}