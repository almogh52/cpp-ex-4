// The library is compiled on debug to the solution debug folder
#pragma comment(lib, "../Debug/streams.lib")

#include "OutStream.h"
#include "FileStream.h"

int main()
{
    char *fileName = (char *)"some_file.txt";

    // Create a stdout stream and print to it the message
    msl::OutStream cout = msl::OutStream();
    cout << "I am the Doctor and I'm " << 1500 << " years old" << msl::endline;

    // Create a file stream to a file and print to it the message
    msl::FileStream fileStream = msl::FileStream(fileName);
    fileStream << "I am the Doctor and I'm " << 1500 << " years old" << msl::endline;

    system("pause");

    return 0;
}