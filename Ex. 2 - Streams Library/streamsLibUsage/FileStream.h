#pragma once
#include <stdlib.h>
#include "OutStream.h"

namespace msl {
    class FileStream : public OutStream
    {
    public:
	FileStream(char *fileName);
	~FileStream();
    };
}


